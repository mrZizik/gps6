package com.zizik.gps6;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.clear();
		menu.add(0, 1, 1, "������");
		menu.add(0, 2, 2, "���������");
		menu.add(0, 3, 3, "���������");
		
		
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
         // TODO Auto-generated method stub

         switch (item.getItemId()) {
         case 1:
                
                 Intent intent = new Intent(this, Kompas.class);
                 startActivity(intent);
                 break;
         
         case 2:
                 
                 Intent intent1 = new Intent(this, Point.class);
                 startActivity(intent1);
                 break;
                 
         case 3:
                 
                 Intent intent2 = new Intent(this, MainActivity.class);
                 startActivity(intent2);
                 break;
               
         }

         return super.onOptionsItemSelected(item);
 }

}
